Novation Launchpad Animator
======
**Novation Launchpad Animator** is a tool for making a lightshow animation for 
different Launchpads. The software provides a UI, similar to the user's launchpad,
and allows the user to fine-tune color and lenght of each button. The program 
will generate a midi file that can be fed into the launchpad by other software.

*Note that the software is currently incomplete*