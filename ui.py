from controller import launchpadInterface
import pygame as pg
import constants as const
import wx


# - CUSTOM WIDGETS -----------------------------------------------------------------------------------------------------
# CUSTOM BUTTON SKELETON || FIXME: Delete later
class MyButton(wx.Control):

    def __init__(self, parent, id, bmp, text):
        wx.Control.__init__(self)

        self.Bind(wx.EVT_LEFT_DOWN, self._onMouseDown)
        self.Bind(wx.EVT_LEFT_UP, self._onMouseUp)
        self.Bind(wx.EVT_LEAVE_WINDOW, self._onMouseLeave)
        self.Bind(wx.EVT_ENTER_WINDOW, self._onMouseEnter)
        self.Bind(wx.EVT_ERASE_BACKGROUND,self._onEraseBackground)
        self.Bind(wx.EVT_PAINT,self._onPaint)

        self._mouseIn = self._mouseDown = False

    def _onMouseEnter(self, event):
        print("enter")
        self._mouseIn = True

    def _onMouseLeave(self, event):
        print("Leave")
        self._mouseIn = False

    def _onMouseDown(self, event):
        print("down")
        self._mouseDown = True

    def _onMouseUp(self, event):
        print("up")
        self._mouseDown = False
        self.sendButtonEvent()

    def sendButtonEvent(self):
        print("send")
        event = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, self.GetId())
        event.SetInt(0)
        event.SetEventObject(self)
        self.GetEventHandler().ProcessEvent(event)

    def _onEraseBackground(self,event):
        print("erase")
        # reduce flicker
        pass

    def _onPaint(self, event):
        print("Paint")
        dc = wx.BufferedPaintDC(self)
        dc.SetFont(self.GetFont())
        dc.SetBackground(wx.Brush(self.GetBackgroundColour()))
        dc.Clear()
        # draw whatever you want to draw
        # draw glossy bitmaps e.g. dc.DrawBitmap
        if self._mouseIn:
            pass# on mouserover may be draw different bitmap
        if self._mouseDown:
            pass # draw different image text

# Test of color swaps
class ColorButton(wx.Control):

    def __init__(self, parent, width, height, brushColor=wx.GREY_BRUSH, id=wx.ID_ANY):
        wx.Control.__init__(self, parent=parent, id=id, size=wx.Size(width, height))

        self.height = height
        self.width = width
        self.Bind(wx.EVT_LEFT_DOWN, self._onMouseDown)
        self.Bind(wx.EVT_LEFT_UP, self._onMouseUp)
        self.Bind(wx.EVT_LEAVE_WINDOW, self._onMouseLeave)
        self.Bind(wx.EVT_ENTER_WINDOW, self._onMouseEnter)
        self.Bind(wx.EVT_ERASE_BACKGROUND,self._onEraseBackground)
        self.Bind(wx.EVT_PAINT,self._onPaint)


        self._mouseIn = self._mouseDown = False
        self.currentColor = brushColor

    def _onMouseEnter(self, event):
        #print("enter")
        #self.currentColor = wx.RED_BRUSH
        #self.Refresh()
        self._mouseIn = True

    def _onMouseLeave(self, event):
        #print("Leave")
        self._mouseIn = False

    def _onMouseDown(self, event):
        #print("down")
        self._mouseDown = True

    def _onMouseUp(self, event):
        #print("up")
        self._mouseDown = False
        self.sendButtonEvent()

    def sendButtonEvent(self):
        #print("send")
        event = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED, self.GetId())
        event.SetInt(0)
        event.SetEventObject(self)
        self.GetEventHandler().ProcessEvent(event)

    def _onEraseBackground(self,event):
        #print("erase")
        # reduce flicker
        pass

    def _onPaint(self, event):
        #print("Paint")
        dc = wx.BufferedPaintDC(self)
        dc.SetFont(self.GetFont())
        dc.SetBackground(wx.Brush(self.GetBackgroundColour()))
        dc.Clear()

        # draw whatever you want to draw
        # draw glossy bitmaps e.g. dc.DrawBitmap
        if self._mouseIn:
            pass# on mouserover may be draw different bitmap
        if self._mouseDown:
            pass # draw different image text

        dc.SetBrush(self.currentColor)

        dc.DrawRoundedRectangle(0, 0, self.width - 5, self.height - 5, 5)


# Button for the pads. Simply a ellipse square that can change colors
class PadButtton(wx.Panel):
    def __init__(self, parent, name, id):
        super().__init__(self, parent)
        self.name = name
        self.taskid = id

# Composite to hold launchpad buttons
class lpButtonGrid(object):
    def __init__(self, parent, buttonGridType, id = wx.ID_ANY):
        self.baseVBox = wx.BoxSizer(wx.VERTICAL)


        if buttonGridType == const.BUTTONGRIDTYPE.S:
            grid_LSbuttons = wx.GridSizer(8, 8, 5, 5)
            grid_topButtons = wx.GridSizer(1, 8, 5, 5)
            grid_sideButtons = wx.GridSizer(8, 1, 5, 5)

            for i in range(0, 8):
                grid_topButtons.Add(ColorButton(parent, 40, 40, brushColor=wx.RED_BRUSH))

            for i in range(0, 8):
                grid_sideButtons.Add(ColorButton(parent, 40, 40, brushColor=wx.RED_BRUSH))

            for i in range(0, 8):
                for j in range(0, 8):
                    grid_LSbuttons.Add(ColorButton(parent, 40, 40))

            self.baseVBox.Add(grid_topButtons)
            self.baseVBox.Add(-1, 25)

            hbox = wx.BoxSizer(wx.HORIZONTAL)

            hbox.Add(grid_LSbuttons)
            hbox.Add(25, -1)
            hbox.Add(grid_sideButtons)

            self.baseVBox.Add(hbox)



    def getLayout(self):
        return self.baseVBox



# ----------------------------------------------------------------------------------------------------------------------


# WINDOW CLASS ---------------------------------------------------------------------------------------------------------
class MainWindow(wx.Frame):

    # All controllers and stuff should be set up in the init
    def __init__(self, parent):
        super(MainWindow, self).__init__(parent, title=const.WINDOW_TITLE, size = (const.WINDOW_WIDTH, const.WINDOW_HEIGHT))
        # Base UI Elements
        self.panel_root = wx.Panel(self)
        self.gridBag_main = wx.GridBagSizer(5,5)


        label_FrameCount = wx.StaticText(self.panel_root, label=const.LABELS.FRAMECOUNT)
        ctrl_FrameCount = wx.StaticText(self.panel_root, label="00000") # FIXME change to editable and let user choose a frame

        label_BPM = wx.StaticText(self.panel_root, label=const.LABELS.BPM)
        ctrl_BPM = wx.StaticText(self.panel_root, label="130") # FIXME change to editable and let user choose a BPM

        label_Device = wx.StaticText(self.panel_root, label=const.LABELS.DEVICE)
        ctrl_ChooseDevice = wx.ComboBox(self.panel_root)

        #label_ColorSelect = wx.StaticText(panel_root, label=const.LABELS.COLOR_SEL)
        self.gridBag_main.Add(label_Device,
                              pos=(0, 0),
                              flag=wx.LEFT,
                              border=0)
        self.gridBag_main.Add(ctrl_ChooseDevice,
                              pos=(0, 1),
                              flag=wx.RIGHT,
                              border=0)

        self.gridBag_main.Add(label_FrameCount,
                              pos=(0, 3),
                              flag=wx.LEFT,
                              border=0)
        self.gridBag_main.Add(ctrl_FrameCount,
                              pos=(0, 4),
                              flag=wx.RIGHT,
                              border=0)

        self.gridBag_main.Add(label_BPM,
                              pos=(0, 5),
                              flag=wx.LEFT,
                              border=0)
        self.gridBag_main.Add(ctrl_BPM,
                              pos=(0, 6),
                              flag=wx.RIGHT,
                              border=0)





        self.panel_root.SetSizer(self.gridBag_main)

        ctrl_ChooseDevice.Bind(wx.EVT_COMBOBOX_DROPDOWN, self._onDeviceSelected)

        launchPadButtons = lpButtonGrid(self.panel_root, const.BUTTONGRIDTYPE.S)

        self.gridBag_main.Add(launchPadButtons.getLayout(),
                              pos=(1, 0),
                              span=(0, 7))

        # self.gridBag_main.ShowItems(True)
        self.gridBag_main.Layout()

        self.Show(True)
        self.Centre()

    #TODO Currently does nothing
    def _onDeviceSelected(self, event):

        #temp:wx.GBSizerItem = self.gridBag_main.FindItemAtPosition((1, 0))
        #if temp != None:
         #   temp.Destroy()
         #   temp.DetachSizer()
         #   print(self.gridBag_main.FindItemAtPosition((1, 0)))


        #self.Refresh()
        pass



    def startView(self):
        pass

    def endView(self):
        pass


# ----------------------------------------------------------------------------------------------------------------------


if __name__=="__main__":
    # View(640, 400).startView()
    app = wx.App(False)
    frame = MainWindow(None)

    app.MainLoop()
