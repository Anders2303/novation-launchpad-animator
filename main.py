import controller
from random import randint
from test import DeviceConnector
import time

print("controller --------------------------------------------------------")

c = controller.LaunchPadS()
for color in c.getColorsDict():
    print(color)
print()
print("Test stuff --------------------------------------------------------")
conn = DeviceConnector()
conn.initMidi()

deviceDict = conn.findDevices()

for key, value in deviceDict.items():
    print(value.toString())
    print("~~~~~~~~~~~~~~~~"*6)

note = 0
duration = 0.025

squares = c.pads
colors = c.colors
for row in squares:
    for sqr in row:
        pass
        #conn.sendMidi(sqr, colors[randint(1, len(colors)-1)]['velocityDec'], duration)
conn.quitMidi()



