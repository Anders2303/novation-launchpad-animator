import pygame.midi as pymidi
import time
import threading

class Device(object):

    def __init__(self, name, interface):
        self.name = name
        self.interface = interface
        self.inputID = -1
        self.outputId = -1
        self.openID = -1

    def setInput(self, id):
        self.inputID = id

    def setOutput(self, id):
        self.outputId = id

    def setOpen(self, id):
        self.openID = id

    def toString(self):
        out = ""
        out += "Device Name :  " + self.name.decode("utf-8") + "\n"
        out += "Interface:     " + self.interface.decode("utf-8") + "\n"

        if self.inputID != -1:
            out += "Input device:  TRUE (" + str(self.inputID) + ")\n"
        else:
            out += "Input device:  FALSE\n"

        if self.outputId != -1:
            out += "Output device: TRUE (" + str(self.outputId) + ")"
        else:
            out += "Output device: FALSE"

        return out


class DeviceConnector(object):
    inputDevices = []
    outputDevices = []
    deviceDict = {}

    def sendMidi(self, note, velocity, duration):
        out = pymidi.Output(7)

        out.note_on(note, velocity)

        time.sleep(duration)

        out.note_off(note, velocity)


    def initMidi(self):
        pymidi.init()

    def quitMidi(self):
        pymidi.quit()

    def findDevices(self):
        print("device list:")
        for i in range(pymidi.get_count()):
            interface = pymidi.get_device_info(i)[0]
            name = pymidi.get_device_info(i)[1]
            input = pymidi.get_device_info(i)[2]
            output = pymidi.get_device_info(i)[3]
            open = pymidi.get_device_info(i)[4]

            if name in self.deviceDict.keys():
                device = self.deviceDict[name]
            else:
                device = Device(name, interface)
                self.deviceDict[name] = device

            if input:
                device.setInput(i)

            if output:
                device.setOutput(i)

        #print(self.deviceDict)
        return self.deviceDict