#import win32com.client as wcc


class launchpadInterface(object):
    pads = []
    color = []

    def getColorsDict(self):
        raise NotImplementedError("Abstract method not implemented")

    def findUSBConnection(self):
        raise NotImplementedError("Abstract method not implemented")


class LaunchPadS(launchpadInterface):

    colors = [
        {
            # 'name':"OFF",
            'velocityHex': 0x0c,
            'velocityDec': 12
        }, {
            # 'name':"Dim Red",
            'velocityHex': 0x0d,
            'velocityDec': 13
        }, {
            # 'name':"Red",
            'velocityHex': 0x0e,
            'velocityDec': 14
        }, {
            # 'name':"Full Red",
            'velocityHex': 0x0f,
            'velocityDec': 15
        },

        {
            # 'name':"OFF",
            'velocityHex': 0x1c,
            'velocityDec': 28
        }, {
            # 'name':"Dim Red",
            'velocityHex': 0x1d,
            'velocityDec': 29
        }, {
            # 'name':"Red",
            'velocityHex': 0x1e,
            'velocityDec': 30
        }, {
            # 'name':"Full Red",
            'velocityHex': 0x1f,
            'velocityDec': 31
        },

        {
            # 'name':"OFF",
            'velocityHex': 0x2c,
            'velocityDec': 44
        }, {
            # 'name':"Dim Red",
            'velocityHex': 0x2d,
            'velocityDec': 45
        }, {
            # 'name':"Red",
            'velocityHex': 0x2e,
            'velocityDec': 46
        }, {
            # 'name':"Full Red",
            'velocityHex': 0x2f,
            'velocityDec': 47
        },

        {
            # 'name':"OFF",
            'velocityHex': 0x2c,
            'velocityDec': 44
        }, {
            # 'name':"Dim Red",
            'velocityHex': 0x2d,
            'velocityDec': 45
        }, {
            # 'name':"Red",
            'velocityHex': 0x2e,
            'velocityDec': 46
        }, {
            # 'name':"Full Red",
            'velocityHex': 0x2f,
            'velocityDec': 47
        },
    ]

    pads = [
        range(0, 8),
        range(16, 24),
        range(32, 40),
        range(48, 56),
        range(64, 72),
        range(80, 88),
        range(96, 104),
        range(112, 120)
    ]

    def getColorsDict(self):
        return self.colors

    def findUSBConnection(self):
        pass


