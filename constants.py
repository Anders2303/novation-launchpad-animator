supportedDevices = ["windows"]
WINDOW_WIDTH = 500
WINDOW_HEIGHT = 520
WINDOW_TITLE = "Novation Launchpad Animator"
BACKGROUND_COLOR = (115, 117, 112)  # A bad guess of the colors the launchpads usually are
PAD_OFF_COLOR = (147, 147, 147)

class BUTTONGRIDTYPE:
    S = 0
    PRO = 1

class LABELS:
    FRAMECOUNT = "Current frame"
    BPM = "BPM"
    DEVICE = "Launchpad"
    COLOR_SEL = "Available colors"